
--- DEUTSCH ---

mSlider ist ein jQuery-Plugin
Autor: Andreas Wulf (http://www.mediafant.de)

Jede Menge Informationen, Beschreibungen, Demo, eine Auflistung sämtlicher Parameter finden Sie unter:
http://sources.mediafant.de/mslider

Das Plugin ist lizensiert unter MIT/GPL.

Der Ordner "demo" und die beiden Dateien "testAll_[VERSION].html", "testMinimal_[VERSION].html" sind nur zur Demo und können problemlos gelöscht werden.



--- ENGLISH ---

mSlider is Plugin for jQuery
Author: Andreas Wulf (http://www.mediafant.de)

A lot of Info, Description, Exapmles, Parameters for Configure you will find on:
http://sources.mediafant.de/mslider

The Plugin mSlider is licend under MIT/GPL.

Folder "demo" and Files "testAll_[VERSION].html", "testMinimal_[VERSION].html" only for demo and testing.
You can delete this folder and Files.

Andreas Wulf
http://www.mediafant.de