/*
 ### jQuery mSlider Plugin v3.2 - 2012-10-12 ###
 * http://www.mediafant.de
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 ###
 Website: http://sources.mediafant.de/mSlider/
*//*
 This script create one or more Slider with individual Configuration. Configuration will read from extern JSON or XML-File.
*/


;(function($) {
	
	$.fn.mSlider = function(  ) {
		return this.each(function(sliderKey,sliderItem){
			$(this).addClass('rendered');
			var myUrl = $(sliderItem).data('mslider-conf-path'),
				myPath,
				fromType = 'xml',
				sliderItemOptions,
				sliderId = this.id,
				$mSlider = $(this),
			
			success = function($mSlider,sliderItemOptions) {
				data = {};
				var path = myUrl.substring(0,myUrl.lastIndexOf("/")+1);
				
				if(myUrl.substring(0,1) === '/') {
					data.basepath = path;
				} else if(myUrl.substring(0,4) === 'http') {
					data.basepath = path;
				} else {
					var lastSlashPos = $(location).attr('pathname').lastIndexOf("/")+1;
					data.basepath = $(location).attr('pathname').substring(0,lastSlashPos) + path;
				}
				data.sliderId = sliderId;
				data.params = $.extend(true, {}, $.fn.mSlider.defaults, sliderItemOptions.basic);
				data.sliderData = sliderItemOptions.slides;
				buildSlider($mSlider,data);
			};
			
			if(myUrl.substring( myUrl.length -5 ) === '.json') {
				fromType = 'json';
			}
			
			if( myUrl.substring(0,4) === 'http') {
				myPath =	"http://query.yahooapis.com/v1/public/yql?"+
							"q=select%20*%20from%20"+fromType+"%20where%20url%3D%22"+
							encodeURIComponent(myUrl)+"%22&format=json'&callback=?";
				$.getJSON(myPath,function(backVar){
					if(backVar.results[0]){
						sliderItemOptions = $.xml2json(backVar.results[0]);
						success($mSlider, sliderItemOptions);
					};
				});
			} else {
				if(fromType === 'json') {
					$.getJSON(myUrl, function(backVar) {
						var myBack = backVar;
						if(typeof(backVar) != 'object') {
							myBack = jQuery.parseJSON(backVar);
						}
						if(myBack.slider === undefined) {
							sliderItemOptions = myBack;
						} else {
							sliderItemOptions = myBack.slider;
						}
						success($mSlider, sliderItemOptions);
					});
				} else {
					$.get(myUrl, function(backVar) {
						sliderItemOptions = $.xml2json(backVar);
						success($mSlider, sliderItemOptions);
					});
				}
			}
		});
	};
	
	
	
	$.fn.mSlider.defaults = {
		autoplayAutorun			: 'true',
		autoplayWaitTimeInit	: 8000,
		mouseoverAutoplayStop	: 'false',
		navigationType			: 'number',
		stageWidth				: 1037,
		stageHeight				: 340,
		navigationDisplayControl: 'true',
		navigationPosition		: 'top',
		slideAnimationTime		: 500,
		slideAnimationEasing	: '',
		pagePos					: 0,
		infoButtonsFadeTime		: 200,
		navigationHeight		: 40,
		navigationButtonHeight	: 30,
		roundedCordner			: 'false',
		pathRelativ2Configfile	: 'false',
		designLayer				: null,
		contentButtons			: {
									animation : {
										duration		: 500,
										easing			: '',
										startPosition	: 300,
										targetPosition	: 0
									}
								}
	};
	
	
	function buildSlider ($mSlider, data) {
		if (typeof mSlider === 'undefined') { var mSlider = {}; }
		if (typeof mSlider.builder === 'undefined') { mSlider.builder = {}; }
		if (typeof mSlider.data === 'undefined') { mSlider.data = data; }
		
		var autoplayStatus = 'true',
			fireSliderChange = {},
			fireAutoplayStatus = {};
		
		if (mSlider.data.params.navigationPosition == 'top') {
			mSlider.data.params.navigationOffset = mSlider.data.params.navigationHeight;
		} else {
			mSlider.data.params.navigationOffset = 0;
		}
		
		fireSliderChange.prepare = $.Callbacks();
		fireSliderChange.change  = $.Callbacks();
		fireSliderChange.end 	 = $.Callbacks();
		fireAutoplayStatus.change = $.Callbacks();
		
		mSlider.builder.stage = function() {
			var $layout = $('<div class="stage"></div>'),
				$stripe = mSlider.builder.stripe(),
				$mask = mSlider.builder.mask(),
				$designLayer = mSlider.builder.designLayer(),
				$controls = mSlider.builder.controls()
			;
			
			$layout.css({height:mSlider.data.params.stageHeight});
			
			$layout.append($stripe);
			$layout.append($mask);
			$layout.append($controls);
			$layout.append($designLayer);
			
			return $layout;
		};
	
		mSlider.builder.setPath = function (myPath,myData) {
			var returnPath = myPath;
			if(myData.params.pathRelativ2Configfile === 'true') {
				if(myPath.substring(0,4) != 'http' && myPath.substring(0,1) != '/') {
					returnPath = myData.basepath + myPath;
				}
			}
			return returnPath;
		};

		
		mSlider.builder.stripe = function() {
			var $layout = $('<div class="stageSlider"></div>'),
			
				change = function(myData,tarPos) {
					var $self = $(this);
					var $sl = $self.find('.stageSlider');
					var xPos = myData.params.stageWidth * tarPos *-1;
					$sl.data('self',$self );
					$self.find('.stageSlider').stop()
						.animate({marginLeft: xPos},
								 myData.params.slideAnimationTime,
								 myData.params.slideAnimationEasing,
								 function(e,k){
									fireSliderChange.end.fireWith($(this).data('self'),[myData,tarPos]);
					})
				}
			;
			fireSliderChange.change.add(change);
			
			$(mSlider.data.sliderData).each(function(pageNo,slideValue){
				var imgPath = mSlider.builder.setPath(slideValue.img,mSlider.data);
				var $slide = $('<div class="pic pics'+pageNo+'">')
					.css({width:mSlider.data.params.stageWidth,height:mSlider.data.params.stageHeight,marginLeft:(pageNo*mSlider.data.params.stageWidth),position:'absolute'});
					$( '<img class="pic'+pageNo+'">').appendTo($slide)
					.one('load',function(){
					})
					.attr('src',imgPath)
					.each(function(){
						if(this.complete) $(this).trigger('load');
				});
				$layout.append($slide);
			});
			$layout.css({marginLeft:((mSlider.data.params.stageWidth * mSlider.data.params.pagePos)*-1)});
			return $layout;
		};
		
		
		mSlider.builder.mask = function() {
			var $layout;
			if(mSlider.data.params.roundedCordner === 'true') {
				$layout = $('<div class="stageMask"></div>');
				$('<div class="corner tl"></div>').appendTo($layout);
				$('<div class="corner tr"></div>').appendTo($layout);
				$('<div class="corner bl"></div>').appendTo($layout);
				$('<div class="corner br"></div>').appendTo($layout);
			}
			return $layout;
		};
		
		mSlider.builder.designLayer = function() {
			var $layout;
			if(mSlider.data.params.designLayer != null) {
				$layout = $('<div class="designLayer '+mSlider.data.params.designLayer.className+'">'+mSlider.data.params.designLayer.code+'</div>');
			}
			return $layout;
		}
		mSlider.builder.controls = function() {
			var $layout = $('<div class="controlContainer"></div>')
				.css({'marginTop':mSlider.data.params.navigationOffset+'px',
					 'height':(mSlider.data.params.stageHeight-mSlider.data.params.navigationHeight) }),
				
			buildSlideControlLeft = function() {
				var $but = $('<div class="but butLeft"></div>').appendTo($layout);
				doButtonAnimation('left',$but);
				$but.bind('click',function(){
					fireSliderChange.prepare.fireWith($mSlider,['back',mSlider.data]);
					fireAutoplayStatus.change.fireWith($mSlider,[mSlider.data,'autoplayTimerReset']);
				});
			},
		
			buildSlideControlRight = function() {
				var $but = $('<div class="but butRight"></div>').appendTo($layout);
				doButtonAnimation('right',$but);
				$but.bind('click',function(){
					fireSliderChange.prepare.fireWith($mSlider,['next',mSlider.data]);
					fireAutoplayStatus.change.fireWith($mSlider,[mSlider.data,'autoplayTimerReset']);
				});
			},
			
			doButtonAnimation = function(typ, $but) {
				if(mSlider.data.params.contentButtons.animation != null) {
					if(typ === 'left') {
						$but.css({'left': mSlider.data.params.contentButtons.animation.startPosition+'px'})
							.animate({'left':mSlider.data.params.contentButtons.animation.targetPosition+'px'},
								 mSlider.data.params.contentButtons.animation.duration,
								 mSlider.data.params.contentButtons.animation.easing);
					} else {
						$but.css({'right': mSlider.data.params.contentButtons.animation.startPosition +'px'})
							.animate({'right': mSlider.data.params.contentButtons.animation.targetPosition +'px'},
								 mSlider.data.params.contentButtons.animation.duration,
								 mSlider.data.params.contentButtons.animation.easing);
					}
				}

			},
			
			buildSlideInfoArea = function() {
				var $but = $('<div class="infoArea">').appendTo($layout),
					
					bindArea = function(myData,tarPos) {
						if(myData != null && myData.sliderData != null) {
							var slideData = myData.sliderData[tarPos],
								$self = $(this),
								$myBut = $self.find('.infoArea');
							if(slideData.link && slideData.link.length > 0) {
								$myBut.click(function(){
									if(slideData.econda && hasEconda === true) {
										emos_userEvent1('marker',slideData.econda);
									}
									fireSliderChange.prepare.fireWith($self,['external',myData]);
									$(location).attr('href',slideData.link);
								});
							} else if(slideData.click && typeof(slideData.click) === 'object') {
								$myBut.click(function(){
									globalEval(slideData.click);
								});
							}
							buildSlideInfoButtons($self, $myBut, myData, tarPos);
						};
					},
					unbindArea = function() {
						var $myBut = $(this).find('.infoArea');
						if($myBut && $myBut.length > 0) {
							$myBut.unbind();
						}
						$myBut.empty();
					}
				;
				fireSliderChange.prepare.add(unbindArea);
				fireSliderChange.end.add(bindArea);
			},
			
			buildSlideInfoButtons = function($self, $butContainer, myData, tarPos) {
				var slideData = myData.sliderData[tarPos],
					
					unbind = function(){
						$butContainer.filter('but').unbind();
					};
				fireSliderChange.prepare.add(unbind);
				
				if(slideData.buttons && typeof(slideData.buttons) === 'object') {
						
					$(slideData.buttons).each(function(butNo,butData){
						var butPath = {};
							butPath.out = mSlider.builder.setPath(butData.imgOut,myData);
							butPath.over = mSlider.builder.setPath(butData.imgOver,myData);
				
						var $but = $('<div class="but but_'+butNo+'" style="position:absolute;top:'+butData.posY+'px;left:'+butData.posX+'px" ><img src="'+butPath.out+'" class="out" alt="info" /><img src="'+butPath.over+'" class="over" alt="info" /></div>')
							.appendTo($butContainer)
							.hide()
							.fadeIn(myData.params.infoButtonsFadeTime);
						
						$but.find('img.over').hide();
						$but.hover(
							function(){
								$(this).find('img.out').hide();
								$(this).find('img.over').show();
							},
							function(){
								$(this).find('img.over').hide();
								$(this).find('img.out').show();
							}
						);
						
						if(butData.link && butData.link.length > 0) {
							$but.click(function(){
								fireSliderChange.prepare.fireWith($self,['external',myData]);
								fireAutoplayStatus.change.fireWith($self,[myData,'autoplayTimerReset']);
								if(slideData.econda && hasEconda === true) {
									emos_userEvent1('marker',slideData.econda);
								}
								$(location).attr('href',butData.link);
							});
						} else if(butData.click && butData.click.length > 0) {
							$but.click(function(){
								fireSliderChange.prepare.fireWith($self,['internal',myData]);
								$.globalEval(butData.click);
							});
								
						}
					});
				};
			};
			
			
			buildSlideControlLeft();
			buildSlideControlRight();
			buildSlideInfoArea();
			mSlider.builder.activateButtonsOver($layout);
			return $layout;
		}
		
				
		
		
		
		mSlider.builder.navigation = function() {
			var bodyMarginTop = (mSlider.data.params.navigationHeight - mSlider.data.params.navigationButtonHeight);
			if(mSlider.data.params.navigationType === 'text') bodyMarginTop = 0;
			var $layout = $('<div class="navigation"></div>').css({'height':mSlider.data.params.navigationHeight}),
				$navBody = $('<div class="body"></div>')
								.css({'marginTop': bodyMarginTop+'px'})
								.appendTo($layout),
				
				createButtonBack = function () {
					var $but = $('<div class="but back">'),
						$butBody = $('<div class="body">&nbsp;</div>').css({'lineHeight':mSlider.data.params.navigationButtonHeight+'px'});
					$butBody.appendTo($but);
					$but.appendTo($navBody);
					$but.bind('click',function(){
						fireSliderChange.prepare.fireWith($mSlider,['back',mSlider.data]);
						fireAutoplayStatus.change.fireWith($mSlider,[mSlider.data,'autoplayTimerReset']);
					});
				},
				
				createButtonNext = function () {
					var $but = $('<div class="but next">'),
						$butBody = $('<div class="body">&nbsp;</div>').css({'lineHeight':mSlider.data.params.navigationButtonHeight+'px'});
					$butBody.appendTo($but);
					$but.appendTo($navBody);
					$but.bind('click',function(){
						fireSliderChange.prepare.fireWith($mSlider,['next',mSlider.data]);
						fireAutoplayStatus.change.fireWith($mSlider,[mSlider.data,'autoplayTimerReset']);
					});
				},
				
				createButtonPlayStop = function () {
					var $but = $('<div class="but stop">'),
						$butBody = $('<div class="body">&nbsp;</div>').css({'lineHeight':mSlider.data.params.navigationButtonHeight+'px'}),
						self = this,
						butStatusChange = function() {
							if(mSlider.data.params.autoplayAutorun === 'true') { // Autorun aktiv / Mouse NOT over Slider
								$but.removeClass('active');
							} else if(mSlider.data.params.autoplayAutorun === 'false') { // Autorun Stop
								$but.removeClass('over');
								$but.addClass('active');
							}
						};
						
					$butBody.appendTo($but);
					$but.appendTo($navBody);
					
					$but.bind('click',function(){
						if(mSlider.data.params.autoplayAutorun === 'true') {
							mSlider.data.params.autoplayAutorun = 'false';
						} else {
							mSlider.data.params.autoplayAutorun = 'true';
						}
						fireAutoplayStatus.change.fireWith($mSlider,[mSlider.data]);
						butStatusChange();
					});
					butStatusChange();
				},
				
				createButtonPages = function() {
					var $pageWrapper = $('<div class="pageWrapper"></div>').appendTo($navBody),
						
						setActivePage = function(myData, tarPos) {
							var $self = $(this),
								$myPageWrapper = $self.find('.pageWrapper');
							
							$myPageWrapper.find('.page').removeClass('active');
							$myPageWrapper.find('.page'+tarPos).addClass('active');
						}
					;
					
					fireSliderChange.change.add(setActivePage);
					
					$(mSlider.data.sliderData).each(function(pageNo,pageVal){
						var $navItem = $('<div>').addClass('but page page'+pageNo),
							$navItemBody = $('<div class="body">').css({'lineHeight':mSlider.data.params.navigationButtonHeight+'px'});
							
						$navItem.append($navItemBody);
						if(mSlider.data.params.navigationType === 'thumbnail' && pageVal.thumbnail != null) {
							var thumb = {};
								thumb.out = mSlider.builder.setPath(pageVal.thumbnail.out,mSlider.data);
								thumb.over = mSlider.builder.setPath(pageVal.thumbnail.over,mSlider.data); ;
								
							$navItemBody.append('<img src="'+thumb.out+'" alt="'+(pageNo+1)+'" class="out" /><img src="'+thumb.over+'" alt="'+(pageNo+1)+'" class="over" />');
						} else if(mSlider.data.params.navigationType === 'text' && pageVal.navigationText != null) {
							$navItemBody.append(pageVal.navigationText);
						} else {
							$navItemBody.append( (pageNo+1) );
						}
						$pageWrapper.append($navItem);
						$navItem.bind('click',function(e){
							if(pageVal.navigationLink) {
								if(pageVal.econda && hasEconda === true) {
									emos_userEvent1('marker',pageVal.econda);
								}
								$(location).attr('href',pageVal.navigationLink);
							} else {
								fireSliderChange.prepare.fireWith($mSlider,['goto', mSlider.data, pageNo] );
								fireAutoplayStatus.change.fireWith($mSlider,[mSlider.data,'autoplayTimerReset']);
							}
						});
					});
				};
				
			if(mSlider.data.params.navigationDisplayControl === 'true') {
				createButtonPlayStop();
				createButtonBack();
				createButtonPages();
				createButtonNext();
			} else {
				createButtonPages();
			}
			mSlider.builder.activateButtonsOver($layout);
			return $layout;
		};
		
		
		
		
		
		
		
		
		// --- Helpers -------
		
		mSlider.builder.activateButtonsOver = function ($obj) {
			$obj.find('.but').hover(
				function() {
					$(this).addClass('over');
				},
				function() {
					$(this).removeClass('over');
				}
			);
		},
		
		
		
		
		mSlider.builder.autoplay = function () {
			
			var status,
			
				autoplayChange = function(myData, intervalReset) {
					var $self = $(this),
						autorun = myData.params.autoplayAutorun,
						autoplayStatus = myData.params.autoplayStatus,
						autoplayWaitTime = myData.params.autoplayWaitTimeInit,
						autoplayOverStop = myData.params.mouseoverAutoplayStop;
						
					if(autoplayStatus === undefined) autoplayStatus = 'true';
					
					if(autorun === 'true' && (autoplayStatus === 'true' || autoplayOverStop === 'false')) { // Autorun aktiv / Mouse NOT over Slider
						status = 'start';
						
						if(myData.params.autoplayTimer) {
							clearInterval(myData.params.autoplayTimer);
						}
						myData.params.autoplayTimer = setInterval(
							function(evt){
								fireSliderChange.prepare.fireWith($self,['next',myData])
							}, autoplayWaitTime
						);
						
					} else if(autorun === 'true' && autoplayStatus === 'false') { // Autorun active / MouseOver Slider
						status = 'pause';
						if(myData.params.autoplayTimer) {
							clearInterval(myData.params.autoplayTimer);
						}
						
					} else if(autorun === 'false') { // Autorun Stop
						status = 'stop';
						if(myData.params.autoplayTimer) {
							clearInterval(myData.params.autoplayTimer);
						}
					}
					
				},
				
				autoplayInitOverStop = function() {
					var $data = $mSlider;
					$mSlider.data('self',$data);
					$mSlider.mouseover(
						function() {
							mSlider.data.params.autoplayStatus = 'false';
							fireAutoplayStatus.change.fireWith($(this).data('self'),[mSlider.data]);
						}
					).mouseout(
						function(){
							mSlider.data.params.autoplayStatus = 'true';
							fireAutoplayStatus.change.fireWith($(this).data('self'),[mSlider.data]);
						}
					)
				}
			;
			fireAutoplayStatus.change.add(autoplayChange);
			
			autoplayInitOverStop();
			fireAutoplayStatus.change.fireWith($mSlider,[mSlider.data]);
		},
		
		
		
		mSlider.builder.init = function() {
			$mSlider.addClass(mSlider.data.params.navigationType);
			$mSlider.css({width:mSlider.data.params.stageWidth,height:mSlider.data.params.stageHeight});
			$mSlider.empty();
		},
		
		mSlider.builder.machine = function() {
			var sliderChange = function(changeTyp, myData, pagePos ) {
				var $self = $(this);
					tarPos = 0;
					
				if(changeTyp === 'external' || changeTyp === 'reset') {
					
				} else {
					if(changeTyp === 'back') {
						tarPos = getTarPos(-1, myData.params.pagePos, myData.sliderData);
					} else if(changeTyp === 'next') {
						tarPos = getTarPos(+1, myData.params.pagePos, myData.sliderData);
					} else if(changeTyp === 'goto') {
						tarPos = parseInt(pagePos);
					} else if (changeTyp === 'internal') {
						tarPos = myData.params.pagePos;
					}
					myData.params.pagePos = tarPos;
					fireSliderChange.change.fireWith($self,[myData, tarPos]);
				}
			},
			
			getTarPos = function(step, pagePos, sliderdata){
				if(sliderdata != null) {
					var sliderLen = sliderdata.length;
					var tarPos = pagePos + step;
					if(tarPos >= sliderLen) {
						tarPos = 0;
					} else if( tarPos < 0 ) {
						tarPos = sliderLen-1;
					}
					return tarPos;
				}
			}
			;
			
			fireSliderChange.prepare.add(sliderChange);
		};
		
		
		var $mSliderStage,
			$mSliderNavigation;
		
		if(typeof(window.emos_userEvent1) == "function") {
			hasEconda = true;
		} else {
			hasEconda = false;
		}
		
		mSlider.builder.init();
		mSlider.builder.machine();
		
		$mSliderStage = mSlider.builder.stage();
		$mSlider.append($mSliderStage);
		
		$mSliderNavigation = mSlider.builder.navigation();
		$mSlider.append($mSliderNavigation);
		
		mSlider.builder.autoplay();
		fireSliderChange.change.fireWith($mSlider,[mSlider.data, mSlider.data.params.pagePos]);
		
	};
})(jQuery);